import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PopupWrapper } from './popupWrapper/PopupWrapper';
import { SimplePopup } from './popupWrapper/popups/SimplePopup';
import { PopupModule } from '@progress/kendo-angular-popup';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    PopupWrapper,
    SimplePopup
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PopupModule,
    BrowserAnimationsModule
  ],
  entryComponents: [
    SimplePopup
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
