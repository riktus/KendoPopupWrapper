import { Component, ElementRef, ContentChildren, QueryList, ViewContainerRef, ComponentFactoryResolver, Type, TemplateRef, ViewChild, Input } from '@angular/core';
import {
  PopupService,
  PopupRef
} from '@progress/kendo-angular-popup';

@Component({
  selector: 'popup-wrapper',
  template: `
    <div #anchor (click)="togglePopup(anchor)">
      <ng-content select="[popupHeader]"></ng-content>
    </div>

    <ng-template #templateRef>
        <ng-content select="[template]"></ng-content>
    </ng-template>
  `
})
export class PopupWrapper {
    private popupRef: PopupRef;

    @ViewChild('templateRef') public templateRef: TemplateRef<any>;
    @Input() data: any;

    constructor(private popupService: PopupService) {}

    public togglePopup(anchor: ElementRef) {
        if (this.popupRef) {
            this.popupRef.close();
            this.popupRef = null;
        } else {
            this.popupRef = this.popupService.open({
            anchor: anchor,
            content: this.templateRef
            });
        }
    }
}