import { Component, ElementRef, TemplateRef, ComponentRef, ContentChildren, ViewChild } from '@angular/core';
import { PopupWrapper } from '../PopupWrapper';

@Component({
  selector: 'simple-popup',
  templateUrl: 'templates.html'
})
export class SimplePopup extends PopupWrapper {
  @ViewChild('SimplePopup') public templateRef: TemplateRef<any>;
}